let car;
let map;

function setup() {
  createCanvas(1200, 800);
  car = new Car();
  map = new Map();
}

function draw() {
  background(0);
  map.show()
  car.update(map);
  car.show();
}


function mousePressed() {
    map.mousePressed();
}

function mouseReleased() {
  map.mouseReleased();
}
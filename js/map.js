class Map {
    constructor() {
        this.paths = [[{x:0,y:0,color:0,weight:0},{x:0,y:height,color:0,weight:0},{x:width,y:height,color:0,weight:0},{x:width,y:0,color:0,weight:0},{x:0,y:0,color:0,weight:0}]];
        this.pathTypes = [99]
        this.currentPath = [];
        this.erasor = false;
        this.numCheckpoints = 0;
        this.start = createVector(width / 2, height / 2);
        this.startDir = 0
        this.startDirXY = createVector(0,-1);
    }

    updateAngle() {
        var target = null;
        for (let i = 0; i< this.paths.length; i++) {
            if (this.pathTypes[i] == 1) {
                target = this.paths[i][floor(this.paths[i].length/2)]
                break;
            }
        }
        if (target) {
            this.startDir = degrees(createVector(0,1).angleBetween(createVector(-target.x+this.start.x,-target.y+this.start.y)));
            this.startDirXY=p5.Vector.fromAngle(radians(this.startDir));
            var x=this.startDirXY.y
            this.startDirXY.y = -this.startDirXY.x;
            this.startDirXY.x = x;
        }
    }
    
    show() {
        colorMode(RGB)
        noFill();
    
        if (mouseIsPressed && !this.erasor && !(this.currentPath.length >= 1 && this.currentPath[this.currentPath.length-1].x == mouseX && this.currentPath[this.currentPath.length-1].y == mouseY)) {
            const point = {
                x: mouseX,
                y: mouseY,
            };
            this.currentPath.push(point);
            if (this.pathTypes[this.pathTypes.length-1] == 1) this.updateAngle()
        }
    
        var checkpointNum = 0;
        for (let i = this.paths.length-1; i>=0; i--) {
            beginShape();
            if (this.pathTypes[i] == 1) {
                if (this.numCheckpoints > 1) stroke(255,checkpointNum/(this.numCheckpoints-1)*255,0);
                else stroke(255,0,0);
                strokeWeight(3);
                checkpointNum++;
            } else {
                stroke(255);
                strokeWeight(3);
            }
            for (let point of this.paths[i]) {
                if ((this.erasor && [0,1].includes(this.pathTypes[i]) && dist(point.x,point.y,mouseX,mouseY) < 5) || (i != this.paths.length-1 && this.paths[i].length == 1)) {
                    this.paths.splice(i,1);
                    if (this.pathTypes[i] == 1) this.numCheckpoints--;
                    this.pathTypes.splice(i,1);
                    break;
                }
                vertex(point.x, point.y);
            }
            endShape();
        }

        stroke(0,255,0)
        fill(0,255,0)
        translate(this.start.x, this.start.y);
        line(0, 0,  this.startDirXY.x * 10,  this.startDirXY.y * 10);
        rotate(this.startDirXY.heading());
        translate(this.startDirXY.mag()*10, 0);
        triangle(0, 5, 0, -5, 10, 0);
        translate(-this.startDirXY.mag()*10,0)
        rotate(-this.startDirXY.heading());
        translate(-this.start.x, -this.start.y);
    }

    mousePressed() {
        this.currentPath = [];
        this.paths.push(map.currentPath);
        if (keyIsDown(90)) {
            this.pathTypes.push(1)
            this.numCheckpoints++;
        } else if (keyIsDown(16)) {
            this.erasor = true;
            this.paths.pop()
        } else if (keyIsDown(88)) {
            this.paths.pop()
            this.start = createVector(mouseX,mouseY)
            this.updateAngle()
            car.setPos(this.start, this.startDir)
        } else {
            this.pathTypes.push(0)
        }
    }  

    mouseReleased() {
        this.erasor = false;
    }
}
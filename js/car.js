class Car {
    constructor(speedStats={},bodyStats={},viewStats={}) {
      // Control position of the car
      this.pos = createVector(width / 2, height / 2);
      this.dir = 0;

      // Controls movement of the car
      this.speedStats = Object.assign({currentSpeed:0,maxSpeed:100,acceleration:20,minSpeed:-20,friction:5, turnSpeed:10}, speedStats)

      // Controls looks of the car
      this.bodyStats = Object.assign({color:[floor(random()*(255-100)+100),floor(random()*(255-100)+100),floor(random()*(255-100)+100)],border:255,width:12,height:20}, bodyStats)

      // Controls vision of the car
      this.viewStats = Object.assign({fov:160,rayCount:100}, viewStats)
      this.viewStats.startAngle = -(180-this.viewStats.fov)/2

      // Setup rays for the car
      this.rays = [];
      this.collisionRays  = [];
      var l = [this.bodyStats.width/2, this.bodyStats.height/2, dist(0,this.bodyStats.width/2,this.bodyStats.height/2,0)]
      this.collisionLengths = [l[0],l[2],l[1],l[2],l[0],l[2],l[1],l[2]]
      for (let a = 0; a < this.viewStats.rayCount; a++) {
        this.rays.push(new Ray(this.pos, 0));
      }
      for (let a = 0; a < 8; a++) {
        this.collisionRays.push(new Ray(this.pos, 0));
      }

      // Tracks the cars progression
      this.totalLaps = 0
      this.currentCheckpoint = 0
    }

    setPos(pos, dir) {
      this.pos.x = pos.x;
      this.pos.y = pos.y;
      this.dir = dir
    }

    collisionDetection(map) {
      for (let i = 0; i < this.collisionRays.length; i++) {
        const ray = this.collisionRays[i];
        const pt = ray.cast(map);
        if (pt) {
          const d = p5.Vector.dist(this.pos, pt);
          if (d < this.collisionLengths[i]) {
            if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4) {
              if (keyIsDown(DOWN_ARROW)) {
                this.speedStats.currentSpeed = max(this.speedStats.currentSpeed-(this.speedStats.acceleration/deltaTime),this.speedStats.minSpeed);
              } else if (!keyIsDown(UP_ARROW)) {
                this.speedStats.currentSpeed = min(this.speedStats.currentSpeed+(this.speedStats.friction/deltaTime),0)
              }
              this.speedStats.currentSpeed=min(this.speedStats.currentSpeed,0);
            } if (i == 0 || i == 5 || i == 6 || i == 7 || i == 4) {
              if (keyIsDown(UP_ARROW)) {
                this.speedStats.currentSpeed = min(this.speedStats.currentSpeed+(this.speedStats.acceleration/deltaTime),this.speedStats.maxSpeed);
              } else if (!keyIsDown(DOWN_ARROW)) {
                this.speedStats.currentSpeed = max(this.speedStats.currentSpeed-(this.speedStats.friction/deltaTime),0)
              }
              this.speedStats.currentSpeed=max(this.speedStats.currentSpeed,0);
            } 
          }
        }
      }
    }

    checkpointDetection(map) {
      for (let i = 0; i < this.collisionRays.length; i++) {
        const ray = this.collisionRays[i];
        const pt = ray.checkpointCast(map, this.currentCheckpoint%map.numCheckpoints);
        if (pt) {
          const d = p5.Vector.dist(this.pos, pt);
          if (d < this.collisionLengths[i]) {
            this.currentCheckpoint++;
            this.totalLaps = (this.currentCheckpoint-1)/map.numCheckpoints;
          }
        }
      }
    }
  
    update(map) {
      this.collisionDetection(map)
      if (keyIsDown(LEFT_ARROW)) {
        this.dir-=this.speedStats.turnSpeed/5;
      } if (keyIsDown(RIGHT_ARROW)) {
        this.dir+=this.speedStats.turnSpeed/5;
      } if (keyIsDown(UP_ARROW)) {
        this.speedStats.currentSpeed = min(this.speedStats.currentSpeed+(this.speedStats.acceleration/deltaTime),this.speedStats.maxSpeed);
      } else if (keyIsDown(DOWN_ARROW)) {
        this.speedStats.currentSpeed = max(this.speedStats.currentSpeed-(this.speedStats.acceleration/deltaTime),this.speedStats.minSpeed);
      } else {
        if (this.speedStats.currentSpeed < 0) {
          this.speedStats.currentSpeed = min(this.speedStats.currentSpeed+(this.speedStats.friction/deltaTime),0)
        } else if (this.speedStats.currentSpeed != 0) {
          this.speedStats.currentSpeed = max(this.speedStats.currentSpeed-(this.speedStats.friction/deltaTime),0)
        }
      }
      this.pos.set(max(0,min(width,this.pos.x+(this.speedStats.currentSpeed*deltaTime/300*sin(radians(this.dir))))),max(0,min(height,this.pos.y-(this.speedStats.currentSpeed*deltaTime/300*cos(radians(this.dir))))))
      for (let i = 0; i < this.viewStats.rayCount; i++) {
        this.rays[i].setAngle(radians(this.viewStats.startAngle+this.dir-(i*(this.viewStats.fov/this.viewStats.rayCount))));
      }
      var corner = atan((this.bodyStats.width/2)/(this.bodyStats.height/2))
      var angle = radians(this.dir)
      for (let a = 0; a < 8; a++) {
        this.collisionRays[a].setAngle(angle);
        if (a ==0 || a == 3 || a == 4) {
          angle -= (radians(90)-corner)
        } else {
          angle -= corner
        }
      }
      this.checkpointDetection(map)
      this.look(map)
    }
  
    look(map) {
      for (let i = 0; i < this.rays.length; i++) {
        const ray = this.rays[i]
        ray.cast(map);
      }
    }
  
    show() {
      for (let ray of this.rays) {
        ray.show();
      } 
      translate(this.pos.x,this.pos.y)
      rotate(radians(this.dir));
      rectMode(CENTER)
      colorMode(RGB);
      fill(this.bodyStats.color);
      stroke(this.bodyStats.border);
      strokeWeight(1);
      rect(0, 0, this.bodyStats.width, this.bodyStats.height)
    }
  }
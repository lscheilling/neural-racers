class Ray {
    constructor(pos, angle) {
      this.pos = pos;
      this.dir = p5.Vector.fromAngle(angle);
      this.d = 0
    }

    setAngle(angle) {
        this.dir = p5.Vector.fromAngle(angle);
    }
  
  
    show() {
        if (this.d > 0) {
            stroke(255);
            strokeWeight(.5);
            push();
            translate(this.pos.x, this.pos.y);
            line(0, 0, this.dir.x * this.d, this.dir.y * this.d);
            pop();
        }
    }

    cast(map) {
        let closest = null;
        let record = Infinity;
        map.paths.forEach((path, index) => {
            if ([0,99].includes(map.pathTypes[index])) {
                for (let i=0; i<path.length-1; i++) {
                    var pt = null;
                    const x1 = path[i].x
                    const y1 = path[i].y;
                    const x2 = path[i+1].x;
                    const y2 = path[i+1].y;
                
                    const x3 = this.pos.x;
                    const y3 = this.pos.y;
                    const x4 = this.pos.x + this.dir.x;
                    const y4 = this.pos.y + this.dir.y;
                    const den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
                    if (den == 0) continue;
                
                    const t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den;
                    const u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den;
                    if (t > 0 && t < 1 && u > 0) {
                        const pt = createVector();
                        pt.x = x1 + t * (x2 - x1);
                        pt.y = y1 + t * (y2 - y1);
                        const d = p5.Vector.dist(this.pos, pt);
                        if (d < record) {
                          record = d;
                          closest = pt;
                        }
                    }
                }
            }
        });
        if (closest) this.d = p5.Vector.dist(this.pos,closest);
        else this.d = 0;
        return closest;
    }

    checkpointCast(map, checkpointNum) {
        let closest = null;
        let record = Infinity;
        var selectedCheckpoint = 0;
        for (let j=0; j<map.paths.length; j++) {
            if (map.pathTypes[j] == 1) {
                if (selectedCheckpoint == checkpointNum) {
                    for (let i=0; i<map.paths[j].length-1; i++) {
                        var pt = null;
                        const x1 = map.paths[j][i].x
                        const y1 = map.paths[j][i].y;
                        const x2 = map.paths[j][i+1].x;
                        const y2 = map.paths[j][i+1].y;
                    
                        const x3 = this.pos.x;
                        const y3 = this.pos.y;
                        const x4 = this.pos.x + this.dir.x;
                        const y4 = this.pos.y + this.dir.y;
                        const den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
                        if (den == 0) continue;
                    
                        const t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den;
                        const u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den;
                        if (t > 0 && t < 1 && u > 0) {
                            const pt = createVector();
                            pt.x = x1 + t * (x2 - x1);
                            pt.y = y1 + t * (y2 - y1);
                            const d = p5.Vector.dist(this.pos, pt);
                            if (d < record) {
                              record = d;
                              closest = pt;
                            }
                        }
                    }
                    break;
                } else {
                    selectedCheckpoint++;
                }
            }
        }
        if (closest) this.d = p5.Vector.dist(this.pos,closest);
        else this.d = 0;
        return closest;
    }
  }